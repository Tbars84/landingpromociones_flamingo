$(document).ready(function(){

	var BannerWith = $('.banner').height();
	$('.banner-fondo').height(BannerWith);
	$('.footer-close').height(BannerWith*0.5);
	iniResponsive( $(window).width() );
    $( window ).resize(function() {
		var BannerWith = $('.banner').height();
		$('.banner-fondo').height(BannerWith);
		$('.footer-close').height(BannerWith*0.5);
        iniResponsive( $(window).width() );
    });

Activaformulario();
if ( $('.respuesta-form') ) {
    $('.respuesta-form').stop().animate({'opacity': 0})
}

$('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});

    function createDropdown() {
        var dropdownCount = 0,
            dropdownIndex = $('.u-customDropdown').length + 5;

        $.each($('.u-customDropdown'), function() {
            var source     = $(this).find('select'),
                selected   = source.find('option[selected]'),
                options    = $('option', source),
                sourceId   = 'dropdownSource-'+ dropdownCount,
                dropdownId = 'dropdownTarget-'+ dropdownCount;

            source.attr('id', sourceId).data('target', dropdownId);
            selected = (selected.attr('selected') === undefined)
                ? source.find('option:first-child')
                : selected;

            $(this).append('<dl id="'+ dropdownId +'" data-source="'+ sourceId +'" class="dropdown" style="z-index: '+ dropdownIndex +';"></dl>');
            $('#'+ dropdownId).append('<dt><a href="#">' + selected.text() +
                '<span class="value">' + selected.val() +
                '</span></a></dt>');
            $('#'+ dropdownId).append('<dd><ul></ul></dd>');
            options.each(function() {
                var optionClass = ($(this).val() === '')
                    ? ' class="is-placeholder"'
                    : '';

                $('#'+ dropdownId +' dd ul').append('<li'+ optionClass +'><a href="#">' +
                    $(this).text() + '<span class="value">' +
                    $(this).val() + '</span></a></li>');
            });

            dropdownCount++;
            dropdownIndex--;
        });
    }

    $(document).on('click', '.dropdown dd ul li a', function(e) {
        e.preventDefault();
        var text    = $(this).html(),
            parent  = $(this).closest('.dropdown'),
            value   = $(this).find('span.value').html();

        $('dt a', parent).html(text).toggleClass('has-selection', (value !== ''));
        $('dd ul', parent).hide();
        var source = parent.data('source');
        $('#'+ source).val(parent.find('span.value').html())
    });

    $(document).on('click', '.dropdown dt a', function(e) {
        e.preventDefault();
        var parent   = $(this).closest('.dropdown'),
            dropdown = parent.find('dd ul');

        if (dropdown.is(':hidden')) {
            $('.dropdown dd ul').hide();
        }
        dropdown.toggle();
    });

    $('.u-customDropdown select').on('change', function() {
        var target  = $(this).data('target'),
            value   = $('option:selected', this).text();

        $('#'+ target +' dt a').html(value);
    });

    $(document).bind('click', function(e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass('dropdown'))
            $('.dropdown dd ul').hide();
    });

    createDropdown();


})

function iniResponsive(Wh){

	$('section').not('.cat , .prod').css({'height': (Wh * 0.562)});

		if (Wh <= 359) {

		}
		if (Wh >= 360 && Wh <= 650) {

		}
		else if (Wh >= 651 && Wh <= 768) {

		}
		else if (Wh >= 769 && Wh <= 1350) {

		}
		else if (Wh >= 1351 && Wh <= 1560) {

		}
		else if (Wh >= 1561) {

		}

}

function Activaformulario(){

    var options = { 
        target:        '',   // target element(s) to be updated with server response 
        success:       showResponse  // post-submit callback 
 
        // other available options: 
        //url:       url         // override for form's 'action' attribute 
        //type:      type        // 'get' or 'post', override for form's 'method' attribute 
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
        //clearForm: true        // clear all form fields after successful submit 
        //resetForm: true        // reset the form after successful submit 
 
        // $.ajax options can be used here too, for example: 
        //timeout:   3000 
    }; 

    // bind to the form's submit event 
    $('#envio-datos').submit(function() { 
        // inside event callbacks 'this' is the DOM element so we first 
        // wrap it in a jQuery object and then invoke ajaxSubmit 
        $(this).ajaxSubmit(options); 
 
        // !!! Important !!! 
        // always return false to prevent standard browser submit and page navigation 
        return false; 

    }); 

}

// post-submit callback 
function showResponse(responseText)  { 
    $('.respuesta-form').empty();
    $('.respuesta-form').stop().animate({'opacity': 0})
    if (responseText == '200') {
        $('.respuesta-form').append('<div class="form-tit-resp"><h5>¡Felicitaciones Ganaste!</h5></div><div class="form-resp"><p>Te esperamos en uno de nuestros almacénes para redimir tu descuento. Disfruta de todos los benecios que te trae la FIATÓN.</p></div>');
        $('.respuesta-form').stop().animate({'opacity': 1})        
    }
    else if(responseText === '300'){
        $('.respuesta-form').append('<div class="form-tit-resp"><h5>¡Hola!</h5></div><div class="form-resp"><p>Tú ya has redimido un desuento, espera la próxima actividad para tener nuevos beneficios</p></div>');
        $('.respuesta-form').stop().animate({'opacity': 1})        
    }
    $('#envio-datos')[0].reset();
    var body = $("html, body");
    body.stop().animate({scrollTop:2000}, '5000', 'swing', function() { 
    });    

} 
