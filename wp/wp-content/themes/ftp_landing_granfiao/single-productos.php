<?php  

get_header();
$descuento = rwmb_meta( 'descuento-prod', $args, $post_id );
$ref = rwmb_meta( 'ref-prod', $args, $post_id );
$descripDesc = rwmb_meta( 'descripcion-desc', $args, $post_id );
$descripProd = rwmb_meta( 'descripcion-prod', $args, $post_id );
$imageneCupon = rwmb_meta('img-banner', $args, $post->ID);
$almacenCheck = rwmb_meta( 'AlmacenCheck', 'type=checkbox_list' );
$cats = get_the_category();

foreach ($cats as $cat) { //go thru to find child one - means cat which has specified parent id
        $child = $cat->term_taxonomy_id;
        $childName = $cat->name;
}

?>
<body>
	<main>
		<div class="banner">
			<div class="redes-sociales">
				<p style="text-align: center; width: 100%;margin-bottom: 2px;color: white">Siguenos en:</p>			
				<a class="red-social" href="https://www.facebook.com/almacenesflamingo/?fref=ts"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/fb.png" alt=""></a>
				<a class="red-social" href="https://www.instagram.com/flamingoalmacenes/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/inst.png" alt=""></a>
				<a class="red-social" href="https://twitter.com/FlamingoMeFia"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/tw.png" alt=""></a>
			</div>
			<div class="logo-flamingo">
				<a href="http://flamingo.com.co/">
					<img src="<?php bloginfo('template_url'); ?>/img/logo-flamingo.png" alt="">				
				</a>
			</div>	
			<div class="logo">
				<img src="<?php bloginfo('template_url'); ?>/img/logo-landing-formulario.png" alt="">
			</div>
			<div class="banner-fondo"></div>				
		</div>
		<section id="productos" class="prod">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 volver">
						Volver a <?php echo '<a href="'.site_url().'/cat-prod/'.$childName.'/">'.$childName.'</a>'; ?>
					</div>
				</div>
				<div class="cupon-cont">
					<div class="cupon-img">
						<?php 
							if ( !empty($imageneCupon) ) {
								foreach ($imageneCupon  as $cupon ) {
						?>
								<img src="<?php echo $cupon['url']; ?>" alt="">
						<?php 
								}
							}
							else{
						?>
								<img src="<?php bloginfo('template_url'); ?>/img/foto-producto-cupon.jpg" alt="">
						<?php 


							}
						?>

					</div>
					<div class="cupon-info">
						<div class="cifra-descuento">
							<p class="bono-de">* Bono de</p>
							<h6><?php echo $descuento; ?></h6>
						</div>
						<p class="ganaste">
							<strong>¡GANASTE!</strong>
							Comienza desde ahora a disfrutar de los grandes benecios que trae la FIATÓN para ti y tu familia.
							<br>
							Descuento válido hasta el 5 de Marzo de 2017.
						</p>
						<h5><?php the_title(); ?> <!-- <small>  Ref. <?php //echo $ref; ?></small> --></h5>
						<p>
							<!-- <?php //echo $descripProd; ?> -->
						</p>
					</div>
				</div>
				<div class="form">  
			    <h1>Para obtener este descuento <strong>déjanos tus datos</strong></h1>
			      
					<form id="envio-datos" action="<?php bloginfo('template_url'); ?>/php/respuesta.php" method="post">
					<div class="top-row">
						<div class="field-wrap">
						  <label>
						    Nombre<span class="req">*</span>
						  </label>
						  <input type="text"  name="nombre" id="nombre" required autocomplete="off" />
						</div>
						<div class="field-wrap">
						  <label>
						    Apellido<span class="req">*</span>
						  </label>
						  <input type="text"  name="apellido" id="apellido" required autocomplete="off"/>
						</div>
					</div>
					<div class="top-row">
						<div class="field-wrap">
						  <label>
						    Tipo Documento<span class="req">*</span>
						  </label>
						  <input type="text"  name="tipoDoc" id="tipoDoc" required autocomplete="off" />
						</div>
						<div class="field-wrap">
						  <label>
						    Número documento<span class="req">*</span>
						  </label>
						  <input type="text"  name="numDoc" id="numDoc" required maxlength="12" autocomplete="off"/>
						</div>
					</div>
				<h5>Fecha de Nacimiento</h5>
					<div class="field-wrap">
					<script src="https://cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
					<script>
						webshims.setOptions('forms-ext', {types: 'date'});
						webshims.polyfill('forms forms-ext');
					</script>
					    <input type="date"  name="fechaNac" id="fechaNac">
					</div>
				<h5>Almacen más cercano</h5>
					<div class="u-customDropdown almacenes">
					  <select  name="almacen" id="almacen" required>
					    	<option value="">Almacén</option>
					    	<?php  
							foreach ($almacenCheck  as $almacen ) {
								$val = str_replace(' ', '-', $almacen);
								echo '<option value="'.$val.'">' .$almacen.'</option>';
							}
							?>
					  </select>
					</div>
					<!-- Squared THREE -->
					<div class="field-wrap">
						<div class="squaredThree">
							<input type="checkbox" value="1" id="squaredThree" name="acepto-terminos" required/>
							<label for="squaredThree"></label>
						</div>
						<div class="squaredInfo">
						Autorizo el tratamiento de mis datos personales. <a href="http://highlander.gvbagencia.co/Flamingo/fiaton/wp-content/themes/ftp_landing_granfiao/img/documentos-verif/autorizacion-datos-personales.pdf">(Descargar las políticas de habeas data)</a>
						</div>
					</div>
					<input type="hidden" value="<?php the_title(); ?>" name="producto" />
					<input type="hidden" value="<?php echo $childName; ?>" name="categoria" />	
					<input type="hidden" value="1701" name="IAction" />

			    	<button type="submit" class="button button-block"/>Redimir Descuento</button>
			      </form>
				</div>
				<div class="sep">
					<img src="<?php bloginfo('template_url'); ?>/img/separador.png" alt="">
				</div>
				<div id="respuesta" class="respuesta-form"></div>

			</div>
		</section>

		<div class="porcentaje-1">
			<img src="<?php bloginfo('template_url'); ?>/img/porcentaje-1.png" alt="">
		</div>
		<div class="porcentaje-2">
			<img src="<?php bloginfo('template_url'); ?>/img/porcentaje-2.png" alt="">			
		</div>
		<div class="burbujas-1">
			<img src="<?php bloginfo('template_url'); ?>/img/burbujas-1.png" alt="">
		</div>
		<div class="burbujas-2">
			<img src="<?php bloginfo('template_url'); ?>/img/burbujas-2.png" alt="">			
		</div>
		<div class="footer-close"></div>
		<div class="footer">
			<div class="container-logos">
			</div>
			<div class="links-info">
				<div class="logos-cont">
					<div class="logo-pat">
						<a href="http://casalindaflamingo.com.co/">
							<img src="<?php bloginfo('template_url'); ?>/img/logo-casaLinda.png" alt="">
						</a>
					</div>
					<div class="logo-pat">
						<a href="http://mefia.flamingo.com.co/">
							<img src="<?php bloginfo('template_url'); ?>/img/logo-meFia.png" alt="">
						</a>
					</div>
					<div class="logo-pat">
						<a href="http://www.paseata.com.co/">
							<img src="<?php bloginfo('template_url'); ?>/img/logo-passeata.png" alt="">	
						</a>
					</div>
				</div>

				<a href="http://highlander.gvbagencia.co/Flamingo/fiaton/wp-content/themes/ftp_landing_granfiao/img/documentos-verif/Fiaton-legales.pdf">Políticas de promociones</a>
			</div>
		</div>
	</main>	
	<footer>
		<!-- CONTENIDO DE FOOTER -->
	</footer>	
</body>
<?php  

get_footer();

?>