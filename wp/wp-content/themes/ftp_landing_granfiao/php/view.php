<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Consultas</title>
	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables_themeroller.css">
	<link rel="stylesheet" type="text/css" href="css/boostrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/boostrap/bootstrap-theme.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="js/jsBoostrap/bootstrap.js"></script>
	
</head>
<body>
	<?php 
	session_start();
	if (isset($_POST['submit']) || $_SESSION['admin'] == true) {
		//	Get info from wordpress
		include '../../../../wp-config.php';
		// Set credentials
		$creds = array(
			'user_login'    => $_POST['user'],
			'user_password' => $_POST['password'],
			'remember'      => false
		);
		// Login
		$user = wp_signon($creds, false);
		// Check login
		if (!is_wp_error($user)) {
			$_SESSION['admin'] = $user->caps["administrator"];
			$conn = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
			if ($conn->connect_error){
				echo "Fallo en la conexión: " . $conn->connect_error;
			}
			$sql = "SELECT * FROM formulario_c";
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {			
	?>
		<h1>Registros</h1>
		<a href="Excel.php" target="_Blank">Descargar en excel</a>
		<br />
		<br />
		<table class="table table-dynamic table-striped">
			<thead>
				<tr>
					<th>Tipo de Documento</th>
					<th>Documento</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Fecha Nacimiento</th>
					<th>Autorizó</th>
					<th>Almacen</th>
					<th>Producto</th>
					<th>Categoria</th>
					<th>Fecha de Registro</th>
					<th>IP de Origen</th>
					<th>Proxy de Solicitud</th>
				</tr>
			</thead>
			<tbody>
				<?php
					while($row = $result->fetch_assoc()) {
						echo "<tr>
								<td>".$row["tipo_documento"]."</td>
								<td>".$row["documento"]."</td>
								<td>".$row["nombre"]."</td>
								<td>".$row["apellido"]."</td>
								<td>".$row["fecha_nacimiento"]."</td>
								<td>".$row["autorizo"]."</td>
								<td>".$row["almacen"]."</td>
								<td>".$row["producto"]."</td>
								<td>".$row["categoria"]."</td>
								<td>".$row["fecha_registro"]."</td>
								<td>".$row["remote_addr"]."</td>
								<td>".$row["http_x_forwarded_for"]."</td>
							  </tr>";
					}
				?>
			</tbody>
		</table>
		<script type="text/javascript">
			$(function(){
				$('.table-dynamic').dataTable({
					"order": [[ 1, "asc" ]],
					"scrollX": true,
					select: true,
					"oLanguage": {
						"oAria": {
							"sSortAscending": ": activar ordenamiento ascendente",	
							"sSortDescending": ": activa ordenamiento descendente"
						},	
						"oPaginate": {
							"sFirst": "Primera",
							"sLast": "Última",
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						},
						"sEmptyTable": "No hay datos disponibles en la tabla",
						"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ filas",
						"sInfoEmpty": "Mostrando 0 a 0 de 0 filas",
						"sInfoFiltered": "(filtradas _MAX_ del total de filas)",
						"sInfoPostFix": "",
						"sDecimal": ".",
						"sThousands": ",",
						"sLengthMenu": "Mostrar _MENU_ filas",
						"sLoadingRecords": "Cargando...",
						"sProcessing": "Procesando...",
						"sSearch": "Buscar:",
						"sSearchPlaceholder": "",
						"sUrl": "",
						"sZeroRecords": "No se encontraron registros coincidentes"
					}
				});
			});
		</script>
		<?php
			} else {
				echo "No Hay Registro(s)";
			}
			$conn->close();
		}else{
			$_SESSION['msg'] = $user->get_error_message();
			if (isset($_SESSION['admin'])) {
				unset($_SESSION['admin']);
			}
		}
	}
	if (!isset($_SESSION['admin']) || $_SESSION['admin'] != true) {
	?>
		<style>
			form {
			    width: 50%;
			    margin: 0 auto;
			    margin-top: 1%;
			}
			label {
				font-family: arial;
				color: #505050;
			}
			input[type="text"], input[type="password"] {
			    display: block;
			    width: 100%;
			    padding: .5rem .75rem;
				font-size: 14px;
				line-height: 2;
			    color: #464a4c;
			    background-color: #fff;
			    background-image: none;
			    -webkit-background-clip: padding-box;
			    background-clip: padding-box;
			    border: 1px solid rgba(0,0,0,.15);
			    border-radius: .25rem;
			    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
			    transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
			    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
			    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
			    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
			}

		</style>
		<form method="POST" action="view.php">

			<label for="user">Usuario: </label>	
			<input type="text" name="user" placeholder="Usuario"><br>
			<label for="password">Contraseña: </label>
			<input type="password" name="password" placeholder="Password"><br>
			
			<input type="submit" class="btn btn-primary" name="submit" value="Iniciar Sesión">
		</form>
	<?php 
		if (isset($_SESSION['msg'])) {
			echo $_SESSION['msg'];
		}
	}		
	?>
</body>
</html>