<?php
	session_start();
	if ($_SESSION['admin'] == true) {
		include '../../../../wp-config.php';
		$conn = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
		if ($conn->connect_error){
			echo "Fallo en la conexión: " . $conn->connect_error;
		}
		$sql = "SELECT tipo_documento `Tipo de Documento`, documento `Documento`, nombre `Nombres`, apellido `Apellidos`, fecha_nacimiento `Fecha Nacimiento`, autorizo `Autorizó`, almacen `Almacen`, producto `Producto`, categoria `Categoria`, fecha_registro `Fecha de Registro`, remote_addr `IP de Origen`, http_x_forwarded_for `Proxy de Solicitud` FROM formulario_c";
		$result = $conn->query($sql);
		$tabla = array();
		while($row = $result->fetch_assoc()) {
			array_push($tabla, $row);
		}

		if($result->num_rows > 0){

			error_reporting(E_ALL);
			ini_set('display_errors', TRUE);
			ini_set('display_startup_errors', TRUE);
			date_default_timezone_set('America/Bogota');
			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			/** Se agrega la libreria PHPExcel */
			require_once 'PHPExcel/PHPExcel.php';

			// Wordpress

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			// Se asignan las propiedades del libro
			$objPHPExcel->getProperties()->setCreator("GVB");//Autor
			$objPHPExcel->getProperties()->setLastModifiedBy("GVB");//Ultimo usuario que lo modificó
			$objPHPExcel->getProperties()->setTitle("Reporte");//Titulo
			$objPHPExcel->getProperties()->setSubject("Reporte");// Asunto
			$objPHPExcel->getProperties()->setDescription("Reporte");
			$tituloReporte = "Registros";

			//arreglo para obtener los titulos(key) del arreglo
			$array = $tabla[0];

			//controlador de la columna
			$i = 'A';

			//controlador de la fila
			$j = 2;

	        //Se Agregan los titulos de las columnas
	        foreach ($array as $key => $value) {
	            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j,$key);
	            //$titulosColumnas[] = $key;
	            $i++;
	        }

	        //se regresa letra
	        $ti = $i;
	        for($t='A'; $t < $ti; $t++){$i = $t;}

	        //Se mezclan las celdas del titulo de excel
	        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:'.$i.'1');

	        // Se agregan los titulos del reporte
	        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',$tituloReporte);

	        //Se agregan los datos
	        foreach ($tabla as $filas) {
	            $j++;
	            $i = 'A';
	            foreach ($filas as $fila => $columna) {
	                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($i.$j,  $columna);
	                $i++;
	            }
	        }
	        $ti = $i;

	        for($t='A'; $t < $ti; $t++){$i = $t;}

	        //Se definen los estilos
	        $estiloTituloReporte = array(
	                'font' => array(
	                'name' => 'Verdana',
	                'bold' => true,
	                'italic' => false,
	                'strike' => false,
	                'size' =>16,
	                'color' => array('rgb' => 'FFFFFF')
	            ),
	            'fill' => array(
	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                'color' => array('rgb' => '305496')
	            ),
	            'borders' => array(
	                'allborders' => array('style' => PHPExcel_Style_Border::BORDER_NONE)
	            ),
	            'alignment' => array(
	                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	                'rotation' => 0,
	                'wrap' => TRUE
	            )
	        );
	        $estiloTituloColumnas = array(
	                'font' => array(
	                'name' => 'Arial',
	                'bold' => true,
	                'color' => array('rgb' => 'FFFFFF')
	            ),
	            'fill' => array(
	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                'rotation' => 90,
	                'color' => array('rgb' => '8EA9DB'),
	            ),
	            'borders' => array(
	                'top' => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
	                    'color' => array('rgb' => '143860')
	                ),
	                'bottom' => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
	                    'color' => array('rgb' => '143860')
	                )
	            ),
	            'alignment' => array(
	                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	                'wrap' => TRUE
	            )
	        );

	        $estiloInformacion = new PHPExcel_Style();
	        $estiloInformacion->applyFromArray(
	            array(
	                'font' => array(
	                    'name' => 'Arial',
	                    'color' => array('rgb' => '000000')
	                ),
	                'fill' => array(
	                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                    'color' => array('rgb' => 'FFFFFF')
	                ),
	                'borders' => array(
	                    'allborders' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('rgb' => '000000')
	                    )
	                )
	            )
	        );

	        //Se aplican los estilos definidos
	        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$i.'1')->applyFromArray($estiloTituloReporte);
	        $objPHPExcel->getActiveSheet()->getStyle('A2:'.$i.'2')->applyFromArray($estiloTituloColumnas);
	        $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A3:".($i.$j));


	        for($k = 'A'; $k <= $i; $k++){
	            $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($k)->setAutoSize(TRUE);
	        }

	        // Se asigna el nombre a la hoja
	        $objPHPExcel->getActiveSheet()->setTitle('Informe');

	        // Se activa la hoja para que sea la que se muestre cuando el archivo se abre
	        $objPHPExcel->setActiveSheetIndex(0);
	        // Se inmoviliza el panel
	        $objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,3);

	        // Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="Reporte'.$tipo.'.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			ob_end_clean();
			$objWriter->save('php://output');
			exit;
		}else{
			echo "No Hay Columnas Que Mostrar";
		}
	}
?>