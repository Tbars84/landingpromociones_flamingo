<?php  
if ( function_exists( 'add_theme_support' ))
add_theme_support( 'post-thumbnails' );


add_filter( 'rwmb_meta_boxes', 'box_personalizados' );
function box_personalizados( $meta_boxes ) {
	//  METABOXES PARA LANDING CONFIGURADO EN PAGINA

	$meta_boxes[] = array(
		'title'      => __( 'Panel administrativo para describir el descuento de cada producto', 'textdomain' ),
		'post_types' => 'productos',
		'fields'     => array(
			array(
				'id'   => 'descuento-prod',
				'name' => __( 'Bono de Descuento', 'textdomain' ),
				'size' => '80',
				'type'    => 'text'
			),
            array(
                'type' => 'divider',
            ),
			array(
				'id'   => 'ref-prod',
				'name' => __( 'Referencia producto', 'textdomain' ),
				'size' => '80',
				'type'    => 'text'
			),
   //          array(
   //              'type' => 'divider',
   //          ),
			// array(
			// 	'id'   => 'descripcion-desc',
			// 	'name' => __( 'Descripción descuento', 'textdomain' ),
			// 	'type'    => 'textarea'
			// ),
   //          array(
   //              'type' => 'divider',
   //          ),
			// array(
			// 	'id'   => 'descripcion-prod',
			// 	'name' => __( 'Descripción producto', 'textdomain' ),
			// 	'type'    => 'textarea'
			// ),
            array(
				'type' => 'heading',
                'after' => '<h2 style="padding: 8px 0px;">La imagen recomendada para el banner del cupón es de 620x480</h2>'
            ),
            array(
                'type' => 'divider',
            ),
			array(
				'id'               => 'img-banner',
				'name'             => esc_html__( 'Imagen Banner de cupón', 'img-banner' ),
				'type'             => 'image_upload',
				// Delete image from Media Library when remove it from post meta?
				// Note: it might affect other posts if you use same image for multiple posts
				'force_delete'     => false,
				// Maximum image uploads
				'max_file_uploads' => 1,
				// Display the "Uploaded 1/2 files" status
				'max_status'       => true,
			),
            array(
                'type' => 'divider',
            ),
			array(
				'name'    => esc_html__( 'Almacenes con Promoción' ),
				'id'      => 'AlmacenCheck',
				'type'    => 'checkbox_list',
				// Options of checkboxes, in format 'value' => 'Label'
				'options' => array(
					'Flamingo Parque Berrio' => esc_html__( 'Flamingo Parque Berrio', 'Fl-parque-berrio' ),
					'Flamingo Bolivar' => esc_html__( 'Flamingo Bolivar', 'Fl-bolivar' ),
					'Flamingo Sucre' => esc_html__( 'Flamingo Sucre', 'Fl-sucre' ),
					'Flamingo Itagui' => esc_html__( 'Flamingo Itagui', 'Fl-itagui' ),
					'Flamingo Bello' => esc_html__( 'Flamingo Bello', 'Fl-bello' ),
					'Flamingo Rionegro' => esc_html__( 'Flamingo Rionegro', 'Fl-rioNegro' ),
					'Flamingo Mayorca' => esc_html__( 'Flamingo Mayorca', 'Fl-mayorca' ),
					'Flamingo Armenia' => esc_html__( 'Flamingo Armenia', 'Fl-armenia' ),
					'Flamingo Pereira' => esc_html__( 'Flamingo Pereira', 'Fl-pereira' ),
					'Flamingo Ibague' => esc_html__( 'Flamingo Ibague', 'Fl-ibague' ),
					'Flamingo Piedecuesta' => esc_html__( 'Flamingo Piedecuesta', 'Fl-piedeCuesta' ),
					'Flamingo Villavicencio' => esc_html__( 'Flamingo Villavicencio', 'Fl-villavicencio' ),
					'Flamingo Bogotá' => esc_html__( 'Flamingo Bogotá', 'Fl-bogota' ),
					'Flamingo Soledad' => esc_html__( 'Flamingo Soledad', 'Fl-soledad' ),
					'Flamingo Soacha' => esc_html__( 'Flamingo Soacha', 'Fl-soacha' ),			
					'Flamingo Gran Plaza del Sol' => esc_html__( 'Flamingo Gran Plaza del Sol', 'Fl-plazadelSol' ),
					'Flamingo Sincelejo' => esc_html__( 'Flamingo Sincelejo', 'Fl-sincelejo' ),					
					'Flamingo Valledupar' => esc_html__( 'Flamingo Valledupar', 'Fl-valledupar' ),					

				),
			),

		),
	);

	return $meta_boxes;
}



?>
