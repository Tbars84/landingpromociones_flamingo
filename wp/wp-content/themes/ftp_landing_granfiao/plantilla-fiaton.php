<?php  
/*
* Template Name: Plantilla Landing Fiaton
* 
* Plantilla para Landing Fiaton
*
* GVB
*/

get_header();

?>
<body>
	<main>
		<div class="banner">
			<div class="redes-sociales">
				<p style="text-align: center; width: 100%;margin-bottom: 2px; color: white">Siguenos en:</p>
				<a class="red-social" href="https://www.facebook.com/almacenesflamingo/?fref=ts"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/fb.png" alt=""></a>
				<a class="red-social" href="https://www.instagram.com/flamingoalmacenes/"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/inst.png" alt=""></a>
				<a class="red-social" href="https://twitter.com/FlamingoMeFia"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/tw.png" alt=""></a>
			</div>
			<div class="logo-flamingo">
				<a href="http://flamingo.com.co/">
					<img src="<?php bloginfo('template_url'); ?>/img/logo-flamingo.png" alt="">				
				</a>
			</div>			
			<div class="logo">
				<img src="<?php bloginfo('template_url'); ?>/img/logo-landing-categorias.png" alt="">
			</div>
			<div class="banner-fondo"></div>				
		</div>
		<div class="dropdown">
			<img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/dropDown-flecha.png" alt="">
		</div>
		<section id="categorias" class="cat">
			<div class="container">
				<div class="row cat-cont">

					<?php
	                $args = array( 'post_type' => 'cat-prod' , 'order'  => 'ASC');
	                $loop = new WP_Query( $args );
	                    while ( $loop->have_posts() ) : $loop->the_post();
	                    	$thumb_id = get_post_thumbnail_id();
	                        $img_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
	                ?>
						<div class="cat-prod">
							<a href="<?php echo get_permalink(); ?>">
								<img class="img-responsive" src="<?php echo $img_url[0];?>" alt="<?php the_title(); ?>" >
							</a>
						</div>
					<?php
					    endwhile;
					?>

				</div>
			</div>
		</section>
		<div class="porcentaje-1">
			<img src="<?php bloginfo('template_url'); ?>/img/porcentaje-1.png" alt="">
		</div>
		<div class="porcentaje-2">
			<img src="<?php bloginfo('template_url'); ?>/img/porcentaje-2.png" alt="">			
		</div>
		<div class="burbujas-1">
			<img src="<?php bloginfo('template_url'); ?>/img/burbujas-1.png" alt="">
		</div>
		<div class="burbujas-2">
			<img src="<?php bloginfo('template_url'); ?>/img/burbujas-2.png" alt="">			
		</div>
		<div class="footer-close"></div>
		<div class="footer">
			<div class="container-logos">

			</div>
			<div class="links-info">
				<div class="logos-cont">
					<div class="logo-pat">
						<a href="http://casalindaflamingo.com.co/">
							<img src="<?php bloginfo('template_url'); ?>/img/logo-casaLinda.png" alt="">
						</a>
					</div>
					<div class="logo-pat">
						<a href="http://mefia.flamingo.com.co/">
							<img src="<?php bloginfo('template_url'); ?>/img/logo-meFia.png" alt="">
						</a>
					</div>
					<div class="logo-pat">
						<a href="http://www.paseata.com.co/">
							<img src="<?php bloginfo('template_url'); ?>/img/logo-passeata.png" alt="">	
						</a>
					</div>
				</div>
				<a href="http://highlander.gvbagencia.co/Flamingo/fiaton/wp-content/themes/ftp_landing_granfiao/img/documentos-verif/Fiaton-legales.pdf">Políticas de promociones</a>
			</div>
		</div>
	</main>	
<?php  

get_footer();

?>
